# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2011-2020, Johan Cwiklinski
# This file is distributed under the same license as the Galette package.
# Weblate <noreply@weblate.org>, 2020.
# Quentin PAGÈS <quentinantonin@free.fr>, 2020.
# Johan Cwiklinski <trasher@x-tnd.be>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Galette 0.9.4\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-25 10:27+0100\n"
"PO-Revision-Date: 2022-07-11 06:34+0000\n"
"Last-Translator: Johan Cwiklinski <trasher@x-tnd.be>\n"
"Language-Team: Occitan <https://hosted.weblate.org/projects/galette/"
"documentation-changelog/oc/>\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.13.1-dev\n"

# 52f4192485954e2cbbea4ce7c2a93b29
#: ../changelog.rst:5 52f4192485954e2cbbea4ce7c2a93b29
msgid "Changelog"
msgstr ""

#: ../changelogs/galette_10.rst:7 f7e339f5c27b411b9a3bdedaeac4801a
msgid "0.9.6.1 -> 1.0.0"
msgstr "0.9.6.1 -> 1.0.0"

# 5cb0b0b683374a5394dd9ce5bc91dcd2
# 37c2b9d1138c499e87a47daded56d83e
# 660b4804a0de4c12b1fb9c4f3fb88c5b
# bec23be8d0094214b506971cc7238840
# 861ee9003f3e46d7814e648a671ba388
# baa52cb9720b4ae3a3ba6731f980b33a
# 8b9b428f831344069ec9a0c4e70fc415
# b51cb82b4a76470ea2880af6d2d9cf0c
# c4d06af7cddb4264983edca969bed8ff
# bb742622e48b44c190fcf18d93fc0b1e
# 5b3c963e765c4ae897f179074cc70a18
# ecc0f7ab222247c9ac03c31d97909caf
# e4637005c3354c47ae8a4c254454e8ca
#: ../changelogs/galette_10.rst:12 5cb0b0b683374a5394dd9ce5bc91dcd2
msgid "Added"
msgstr ""

#: ../changelogs/galette_10.rst:14 90e39ee8ae384216b9a1816d1211e496
msgid "Modern UI"
msgstr ""

#: ../changelogs/galette_10.rst:15 c60cf0b056c247968c4dfe1e8ed238d9
msgid ""
"Major improvements on UI/UX and also responsiveness (`#1611 <https://bugs."
"galette.eu/issues/1611>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:16 81f5d2f872f24bbca672f432e3694b76
msgid "Use of YAML files instead of XML for exports configuration"
msgstr ""

#: ../changelogs/galette_10.rst:17 8d5a9fbb6ac8424ab5cf014e02f24445
msgid ""
"New preference to show/hide borders around PDF member cards (`#184 <https://"
"bugs.galette.eu/issues/184>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:18 427d943461cf4b7987024b07d8293da9
msgid "WebP image support (`#1681 <https://bugs.galette.eu/issues/1681>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:19 0d934e8059e64803ac5cc67207c1dfef
msgid ""
"Removed free search on advanced search (`#1684 <https://bugs.galette.eu/"
"issues/1684>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:20 ef79678c4973494d92ca68864b4c2a57
msgid ""
"Check for minimal database version at install (`#1725 <https://bugs.galette."
"eu/issues/1725>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:21 c60cf0b056c247968c4dfe1e8ed238d9
msgid ""
"Resize and crop member picture to a fixed ratio (`#1717 <https://bugs."
"galette.eu/issues/1717>`_)"
msgstr ""

# 9ce8f744c7c84e66987a28e1a4c26569
# edbdcbb6b6a64d95b69b505b28ca605f
# 15af399a691e47039d5ed63901d65085
# da718fbfc4af4b4596ea59e8032b4a5a
# 3c2e63aba20a46ab9294bcb632eb2b25
# 29e1ca171aa74430b223a5be7a43d9ab
# 1648dc1cc0064595ad1da91d15845c46
# 035eb6f9f65d434a92913bdd257cf74f
# 9a2d960ef83b4b4599bf46f613ff0963
# ad8915a2cddd482fa3d65a348346883f
# d15cde785ff44fe799f2cf4ad678e4ec
# 6981fda5bf7241fc91e1c5792e843544
# 67232b1c72244281b3bbfe51881bc130
#: ../changelogs/galette_10.rst:26 9ce8f744c7c84e66987a28e1a4c26569
msgid "Fixed"
msgstr ""

#: ../changelogs/galette_10.rst:28 0d934e8059e64803ac5cc67207c1dfef
msgid ""
"Update issues (not defined constants) (`#1615 <https://bugs.galette.eu/"
"issues/1615>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:29 c60cf0b056c247968c4dfe1e8ed238d9
msgid ""
"Fatal error when cookie not set after login (`#1617 <https://bugs.galette.eu/"
"issues/1617>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:30 427d943461cf4b7987024b07d8293da9
msgid ""
"Sort members by status  (`#1618 <https://bugs.galette.eu/issues/1618>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:31 e40ebf19d4f14628a500813651fb01a4
msgid ""
"Several PHP 8.1 compatibility fixes (`#1629 <https://bugs.galette.eu/"
"issues/1629>`_, `#1655 <https://bugs.galette.eu/issues/1655>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:32 ef79678c4973494d92ca68864b4c2a57
msgid ""
"Groups manager cannot edit their own information (`#1635 <https://bugs."
"galette.eu/issues/1635>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:33 ef79678c4973494d92ca68864b4c2a57
msgid ""
"Inconsistent count and display of reminders members (`#1491 <https://bugs."
"galette.eu/issues/1491>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:34 ef79678c4973494d92ca68864b4c2a57
msgid ""
"Minimum PHP version not displayed on compat page (`#1682 <https://bugs."
"galette.eu/issues/1682>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:35 ef79678c4973494d92ca68864b4c2a57
msgid ""
"Simple members can't access their list of contributions (`#1675 <https://"
"bugs.galette.eu/issues/1675>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:36 427d943461cf4b7987024b07d8293da9
msgid ""
"Contributions mass removal (`#1661 <https://bugs.galette.eu/issues/1661>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:37 0d934e8059e64803ac5cc67207c1dfef
msgid ""
"Disable inline images in mailings (`#1659 <https://bugs.galette.eu/"
"issues/1659>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:38 ef79678c4973494d92ca68864b4c2a57
msgid ""
"Issue editing members with wrong values imported in dynamic choice fields "
"(`#1650 <https://bugs.galette.eu/issues/1650>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:39 c60cf0b056c247968c4dfe1e8ed238d9
msgid ""
"Parent group removed when a manager edit a group (`#1648 <https://bugs."
"galette.eu/issues/1648>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:40 e40ebf19d4f14628a500813651fb01a4
msgid ""
"Fix logo size on member card (`#1626 <https://bugs.galette.eu/issues/1626>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:41 e40ebf19d4f14628a500813651fb01a4
msgid ""
"Fix timeout using logo on PDF member cards (`#1726 <https://bugs.galette.eu/"
"issues/1726>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:42 ef79678c4973494d92ca68864b4c2a57
msgid ""
"Fix dynamic files on contributions and transactions (`#1697 <https://bugs."
"galette.eu/issues/1697>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:43 e40ebf19d4f14628a500813651fb01a4
msgid ""
"Drop required fields on PDF member cards (`#781 <https://bugs.galette.eu/"
"issues/781>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:44 c60cf0b056c247968c4dfe1e8ed238d9
msgid ""
"Parent group can be lost when a groupmanager edits a group (`#1708 <https://"
"bugs.galette.eu/issues/1708>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:45 ef79678c4973494d92ca68864b4c2a57
msgid ""
"Mass add contribution fail if data is missing (`#1694 <https://bugs.galette."
"eu/issues/1694>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:46 ef79678c4973494d92ca68864b4c2a57
msgid ""
"Dynamic contribution fields not rendered on advanced search (`#1693 <https://"
"bugs.galette.eu/issues/1693>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:47 ef79678c4973494d92ca68864b4c2a57
msgid ""
"Dynamic contributions choice fields on advanced search fail using postgres "
"(`#1692 <https://bugs.galette.eu/issues/1692>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:48 ef79678c4973494d92ca68864b4c2a57
msgid ""
"Several minor issues with RTL languages on PDF generation (`#1727 <https://"
"bugs.galette.eu/issues/1727>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:49 ef79678c4973494d92ca68864b4c2a57
msgid ""
"Issues on transactions search with some date formats (`#1731 <https://bugs."
"galette.eu/issues/1731>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:50 ef79678c4973494d92ca68864b4c2a57
msgid ""
"Selected members were not reset creating a new mailing (`#1742 <https://bugs."
"galette.eu/issues/1742>`_)"
msgstr ""

# d217a4ec7c9b4dc7b6230b5e17b8c11e
# 0dec2006cff143eda54fd8b2a312a814
# 1bcd44e56075482ea69e49ed8647c353
# abfad405b4f048d5ae4b278b5a85d648
# ec158439b86340f0b08109665f1348b4
# e52a6f4e7f334983b6a48ed8519a4aac
# 77cd62f8ddae4136807346fd31c6eb14
# 746f9e3ee5a04f09ae8224ec79ee312e
# 9e8386df88a24a26a51a72f59d3fc6ff
#: ../changelogs/galette_10.rst:55 d217a4ec7c9b4dc7b6230b5e17b8c11e
msgid "Under the hood..."
msgstr ""

#: ../changelogs/galette_10.rst:57 14d9ff3f487a455389cd2993957059bd
msgid ""
"Template rendering is now assumed by `Twig <https://twig.symfony.com/>`_ "
"instead of `Smarty <https://smarty.net/>`_ (`#1619 <https://bugs.galette.eu/"
"issues/1619>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:58 732dfb1472734b88a75c1f3d8f76e298
msgid ""
"Use of `Fomantic UI <https://fomantic-ui.com/>`_ framework for whole display "
"(`#1324 <https://bugs.galette.eu/issues/1324>`_)"
msgstr ""

# d52ea576a2ca4063896b19ca385b6ba1
# d57bb274bb344178963fbce5c9b51b7d
#: ../changelogs/galette_10.rst:59 d52ea576a2ca4063896b19ca385b6ba1
msgid "Update third party libraries"
msgstr ""

#: ../changelogs/galette_10.rst:60 ef79678c4973494d92ca68864b4c2a57
msgid ""
"No longer use atoum (dead project) for testing (`#1674 <https://bugs.galette."
"eu/issues/1674>`_)"
msgstr ""

#: ../changelogs/galette_10.rst:61 e40ebf19d4f14628a500813651fb01a4
msgid "LibreJS compatibility (`#1642 <https://bugs.galette.eu/issues/1642>`_)"
msgstr ""

# 3ce3a6271f5740ca98f31c67eda6c1b2
#: ../changelog.rst:12 3ce3a6271f5740ca98f31c67eda6c1b2
msgid "Legacy versions"
msgstr ""

# 4614ee4a7acf46159bd0d42a0c562754
#~ msgid "Plugins"
#~ msgstr "Extensions"
